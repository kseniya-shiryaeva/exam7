import {nanoid} from "nanoid";
import {useState} from "react";
import Order from "./components/Order/Order";
import AddBlock from "./components/AddBlock/AddBlock";
import Dish from './assets/Dish.svg';
import Drink from './assets/Drink.svg';
import './App.css';

const DISHES = [
  {key:nanoid(), type: Dish, name: 'Hamburger', price: 80},
  {key:nanoid(), type: Drink, name: 'Coffee', price: 70},
  {key:nanoid(), type: Dish, name: 'Cheeseburger', price: 90},
  {key:nanoid(), type: Drink, name: 'Tea', price: 50},
  {key:nanoid(), type: Dish, name: 'Fries', price: 45},
  {key:nanoid(), type: Drink, name: 'Cola', price: 40}
];

function App() {
  const [orderedDishes, setOrderedDishes] = useState([]);
  const [total, setTotal] = useState(0);

  const removeItem = e => {
      const key = e.target.id;
      setOrderedDishes(orderedDishes.map(item => {
          if (item.key === key) {
              item.count--;
              setTotal(total - item.price);
          }
          return item;
      }).filter(item => item.count > 0));
  }

  const addDishToOrder = e => {
      e.stopPropagation();
      const key = e.currentTarget.id;
      const isInList = orderedDishes.filter(item => item.key === key).length;
      if (isInList) {
          setOrderedDishes(orderedDishes.map(item => {
              if (item.key === key) {
                  item.count ++;
                  setTotal(total + item.price);
              }
              return item;
          }))
      } else {
          const [newElement] = DISHES.filter(d => d.key === key).map(item => {
              return {key:item.key, name: item.name, price: item.price, count: 1};
          });
          setTotal(total + newElement.price);
          setOrderedDishes(prev => [...prev, newElement]);
      }
  }

  return (
    <div className="App container">
      <div className="col left-col">
          <Order orderedDishes={orderedDishes} removeItem={removeItem} total={total} />
      </div>
      <div className="col right-col">
          <AddBlock dishes={DISHES} addDishToOrder={e => addDishToOrder(e)} />
      </div>
    </div>
  );
}

export default App;
