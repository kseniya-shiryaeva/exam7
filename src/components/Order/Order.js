import React from 'react';
import OrderItem from "../OrderItem/OrderItem";
import Total from "../Total/Total";
import './Order.css';

const Order = ({orderedDishes, removeItem, total}) => {
    let order = <div><p>Order is empty!</p><p>Please add some items!</p></div>;

    if (orderedDishes.length > 0) {
        order = <div className="order_items">
                    {orderedDishes.map(item => (
                        <OrderItem id={item.key} name={item.name} count={item.count} price={item.price} removeItem={removeItem} />
                    ))}
                    <Total total={total} />
                </div>;
    }

    return (
        <fieldset>
            <legend>Order Details:</legend>
            {order}
        </fieldset>
    );
};

export default Order;