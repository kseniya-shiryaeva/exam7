import React from 'react';
import './OrderItem.css';

const OrderItem = ({id, name, price, count, removeItem}) => {
    return (
        <div className="OrderItem">
            <span>{name}</span>
            <span>x {count}</span>
            <span>{price} KGS</span>
            <span className="remove_item_button" id={id} onClick={removeItem}>X</span>
        </div>
    );
};

export default OrderItem;