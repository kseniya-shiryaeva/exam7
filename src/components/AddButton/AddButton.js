import React from 'react';
import './AddButton.css';

const AddButton = ({id, type, name, price, addDishToOrder}) => {
    return (
        <div className="AddButton" id={id} onClick={addDishToOrder}>
            <img src={type} alt={name} className="type_image" />
            <span className="dishTitle">{name}</span>
            <span className="dishPrice">Price: {price} KGS</span>
        </div>
    );
};

export default AddButton;