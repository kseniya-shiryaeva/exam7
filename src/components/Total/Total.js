import React from 'react';
import './Total.css';

const Total = ({total}) => {
    return (
        <div className="Total">
            <span>Total price:</span>
            <span>{total} KGS</span>
        </div>
    );
};

export default Total;