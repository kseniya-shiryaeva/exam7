import React from 'react';
import AddButton from "../AddButton/AddButton";
import './AddBlock.css';

const AddBlock = ({dishes, addDishToOrder}) => {
    return (
        <fieldset>
            <legend>Add Items:</legend>
            <div className="AddBlock">
                {dishes.map(item => (
                    <div className="col-50">
                        <AddButton id={item.key} name={item.name} price={item.price} type={item.type} addDishToOrder={addDishToOrder} />
                    </div>
                ))}
            </div>
        </fieldset>
    );
};

export default AddBlock;